package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.commands.AddSheetCommand;
import ar.fiuba.tdd.tp1.commands.SetValueCommand;
import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatter.NumberFormatter;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;


/**
 * Created by pc on 17/10/15.
 */
public class JsonWriterTest {
    private static final double DELTA = 0.00001;
    private JsonWriter gson = new JsonWriter("Spreadsheet.jss");
    Spreadsheet spreadsheet = new Spreadsheet("hola");

    public void setUp() {
        CommandManager commandManager = new CommandManager();
        AddSheetCommand addSheetCommand = new AddSheetCommand(spreadsheet,"Prueba");
        commandManager.executeCommand(addSheetCommand);
        SetValueCommand svc = new SetValueCommand(spreadsheet,"default","A1","5");
        commandManager.executeCommand(svc);
        SetValueCommand svc1 = new SetValueCommand(spreadsheet,"Prueba","A2","2");
        commandManager.executeCommand(svc1);
        SetValueCommand svc2 = new SetValueCommand(spreadsheet,"Prueba","A3","=A2+1");
        commandManager.executeCommand(svc2);
        SetValueCommand svc3 = new SetValueCommand(spreadsheet,"Prueba","A5","=A3+1");
        commandManager.executeCommand(svc3);
        SetValueCommand svc4 = new SetValueCommand(spreadsheet,"Prueba","B1", "=A5+1");
        commandManager.executeCommand(svc4);
        SetValueCommand svc5 = new SetValueCommand(spreadsheet,"Prueba","B2","=B1+1");
        commandManager.executeCommand(svc5);

        Sheet sheet = spreadsheet.getSheet("default");
        Cell cell = sheet.getCell("A1");
        cell.setCellType("Currency");
        cell.setFormatter(new MoneyFormatter("$",0));
        //Sheet sheet1 = spreadsheet.getSheet("Prueba");
        //Cell cell1 = sheet1.getCell("A2");
        //cell1.setCellType("Number");
        //cell1.setFormatter(new NumberFormatter(2));
    }

    @Test
    //@Ignore
    public void writeAndReadaJson() throws IOException {
        setUp();
        gson.write(spreadsheet);

        SpreadsheetJson spreadsheetJson = gson.read();
        Spreadsheet spreadsheet = new Spreadsheet("hola");
        spreadsheet.importSpreadsheet(spreadsheetJson);

        assertEquals("$ 5", spreadsheet.getValueAsString("default", "A1"));
        assertEquals(2, spreadsheet.getValueFromCellInSheet("Prueba", "A2"), DELTA);
        assertEquals(3, spreadsheet.getValueFromCellInSheet("Prueba", "A3"), DELTA);
        assertEquals(4, spreadsheet.getValueFromCellInSheet("Prueba","A5"),DELTA);
        assertEquals(6, spreadsheet.getValueFromCellInSheet("Prueba","B2"),DELTA);


    }



}