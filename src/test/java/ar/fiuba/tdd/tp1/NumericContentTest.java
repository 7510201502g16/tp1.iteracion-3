package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.NumericContent;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by pc on 27/09/15.
 */
public class NumericContentTest {
    private static final double DELTA = 0.00001;

    @Test
    public void createNumericContent() {
        NumericContent numeric = new NumericContent(5);
        assertNotNull(numeric);
    }

    @Test
    public void getValue() {
        NumericContent numericContent = new NumericContent(5);
        assertEquals(5,numericContent.getNumericValue(),DELTA);
    }

}