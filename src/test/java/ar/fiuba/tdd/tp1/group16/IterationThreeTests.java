package ar.fiuba.tdd.tp1.group16;

import ar.fiuba.tdd.tp1.acceptance.driver.*;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by matias on 11/1/15.
 */
public class IterationThreeTests {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetDriver();
    }

    @Test
    public void powerSimpleTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "3");
        testDriver.setCellValue("tecnicas", "default", "B2", "3");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 ^ B2");

        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void powerCombinedTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "3");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2 ^ A1");
        //Operations in this ugly tp are evaluated in the order they are entered and not in the proper order
        assertEquals(125, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void leftFunctionTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "B1", "= LEFT(A1,3)");
        assertEquals("hol", testDriver.getCellValueAsString("tecnicas", "default", "B1"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void leftFunctionWrongSecondArgumentTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "B1", "= LEFT(A1,caca)");
        assertEquals("hol", testDriver.getCellValueAsString("tecnicas", "default", "B1"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void leftFunctionWrongNumberOfArgumentsTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "B1", "= LEFT(A1,caca,cacota)");
        assertEquals("hol", testDriver.getCellValueAsString("tecnicas", "default", "B1"));
    }

    @Test
    public void rightFunctionTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "B1", "= RIGHT(A1,3)");
        assertEquals("ola", testDriver.getCellValueAsString("tecnicas", "default", "B1"));
    }

    @Test
    public void todayFunctionTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY()");
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        assertEquals(dateFormat.format(date), testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void todayFunctionWithArgumentsTest() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY(A2)");
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        assertEquals(dateFormat.format(date), testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
}
