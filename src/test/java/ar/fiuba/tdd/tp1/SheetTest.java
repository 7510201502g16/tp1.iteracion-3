package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.NumericContent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SheetTest {
    private static final double DELTA = 0.00001;

    private Spreadsheet spreadsheet;
    private Sheet sheet;

    @Before
    public void setUp() {
        this.spreadsheet = new Spreadsheet("");
        this.sheet = new Sheet();
        this.spreadsheet.addSheet(sheet);
    }

    @Test
    public void getCell() {
        String cellPosition = "A1";
        Cell cell = sheet.getCell(cellPosition);
        float value = spreadsheet.getValueFromCell(cellPosition);
        assertEquals(0, value, DELTA);
    }

    @Test
    public void putValue() {
        sheet.putValue("" +  "A1",new NumericContent(5));
        Cell cell = sheet.getCell("A1");
        assertEquals(5, spreadsheet.getValueFromCell("A1"), DELTA);
    }


}
