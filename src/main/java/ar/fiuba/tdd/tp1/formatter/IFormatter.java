package ar.fiuba.tdd.tp1.formatter;

/**
 * Created by pc on 27/10/15.
 */
public interface IFormatter {

    public String format(String string);

    public String getName();

    public String getFormatValue();
}
