package ar.fiuba.tdd.tp1.formatter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pc on 27/10/15.
 */
public class MoneyFormatter implements IFormatter{
    @Expose @SerializedName("Money.Symbol")
    String moneyType;
    NumberFormatter numberFormatter;

    public MoneyFormatter(String moneyType , int decimals) {
        this.moneyType = moneyType;
        this.numberFormatter = new NumberFormatter(decimals);

    }

    public String getMoneyType() {
        return this.moneyType;
    }

    public String format(String numberAsString) {
        String check = numberFormatter.format(numberAsString);
        if ( check.equals("Error:BAD_CURRENCY") ) {
            return "Error:BAD_CURRENCY";
        }
        String result = moneyType + " " + numberFormatter.format(numberAsString);
        return result;

    }

    @Override
    public String getName() {
        return "Money";
    }

    @Override
    public String getFormatValue() {
        return moneyType;
    }
}
