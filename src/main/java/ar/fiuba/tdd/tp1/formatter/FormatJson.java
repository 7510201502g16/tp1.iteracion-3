package ar.fiuba.tdd.tp1.formatter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pc on 29/10/15.
 */
public class FormatJson {

    @Expose @SerializedName("Date.Format")
    private String date = null;

    @Expose @SerializedName("Money.Format")
    private String money = null;

    @Expose @SerializedName("Number.Decimal")
    private String decimals = null;

    public void setMoney( String money) {
        this.money = money;
    }

    public void setDate( String date ) {
        this.date = date;

    }

    public void setDecimals(String decimals) {
        this.decimals = decimals;
    }

    public IFormatter getFormatter() {
        if (date != null) {
            DateFormatter df = new DateFormatter(date);
            return df;
        } else if (money != null) {
            MoneyFormatter mf = new MoneyFormatter(money, 0);
            return mf;
        } else if (decimals != null) {
            NumberFormatter nf = new NumberFormatter(Integer.parseInt(decimals));
            return nf;
        } else {
            return null;
        }
    }


}
