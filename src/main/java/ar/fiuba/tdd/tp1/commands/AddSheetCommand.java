package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Spreadsheet;

/**
 * Created by ezequiel on 11/10/15.
 */
public class AddSheetCommand implements ReversibleCommand {

    private final Spreadsheet spreadsheet;
    private final String sheetName;

    public AddSheetCommand(Spreadsheet spreadsheet, String sheetName) {
        this.spreadsheet = spreadsheet;
        this.sheetName = sheetName;
    }

    @Override
    public String execute() {

        spreadsheet.addSheet(new Sheet(sheetName));
        return null;
    }

    @Override
    public void undo() {
        spreadsheet.removeSheet(sheetName);
    }

    @Override
    public void redo() {
        execute();
    }
}
