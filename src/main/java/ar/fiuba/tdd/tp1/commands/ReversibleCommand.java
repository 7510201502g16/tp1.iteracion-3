package ar.fiuba.tdd.tp1.commands;

/**
 * Created by ezequiel on 11/10/15.
 */
public interface ReversibleCommand {

    String execute();

    void undo();

    void redo();
}
