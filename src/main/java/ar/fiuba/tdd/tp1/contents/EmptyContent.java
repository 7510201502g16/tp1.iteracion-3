package ar.fiuba.tdd.tp1.contents;

/**
 * Created by ezequiel on 27/09/15.
 */
public class EmptyContent implements IContent {
    @Override
    public String getValue() {
        return "";
        //Integer.toString(0);
    }

    @Override
    public String getRaw() {
        return "";
    }
}
