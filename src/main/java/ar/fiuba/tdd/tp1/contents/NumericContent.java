package ar.fiuba.tdd.tp1.contents;

import ar.fiuba.tdd.tp1.FloatUtil;

/**
 * Created by ezequiel on 27/09/15.
 */
public class NumericContent implements IContent {

    private float value;

    public NumericContent(float newValue) {
        value = newValue;
    }

    @Override
    public String getValue() {
        return FloatUtil.toString(value);
        /*if(value == (long) value) {
            return String.format("%d", (long) value);
        } else {
            return String.format("%s",value);
        }*/
    }

    @Override
    public String getRaw() {
        return FloatUtil.toString(value);
    }

    public float getNumericValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}