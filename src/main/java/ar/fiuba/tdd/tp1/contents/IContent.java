package ar.fiuba.tdd.tp1.contents;

/**
 * Created by ezequiel on 27/09/15.
 */
public interface IContent {
    String getValue();

    String getRaw();
}
