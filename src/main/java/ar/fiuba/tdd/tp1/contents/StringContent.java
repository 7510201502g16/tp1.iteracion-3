package ar.fiuba.tdd.tp1.contents;

/**
 * Created by fran on 09/10/15.
 */
public class StringContent implements IContent {
    String value;

    public StringContent(String value) {
        this.value = value;
    }

    @Override
    public String getValue() throws IllegalArgumentException {
        return value;
    }

    @Override
    public String getRaw() {
        return value;
    }

}