package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

/**
 * Created by matias on 11/2/15.
 */
public abstract class SubstringFunction implements Function {

    protected String firstArgument;
    protected Integer secondArgument;

    protected void validateArguments(String argument, DependencyHelper helper) throws IllegalArgumentException {
        String[] arguments =  argument.split(",");
        if (arguments.length != 2) {
            throw new IllegalArgumentException();
        }
        this.firstArgument = helper.getValueAsString(arguments[0]);
        this.secondArgument = castSecondArgument(arguments[1]);
    }


    private static Integer castSecondArgument(String argument) {
        try {
            Integer value = Integer.valueOf(argument);
            return value;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
}
