package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.util.ArrayList;

/**
 * Created by matias on 11/2/15.
 */
public class LeftFunction extends SubstringFunction {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalArgumentException {
        super.validateArguments(argument, helper);
        return this.firstArgument.substring(0,this.secondArgument);
    }

}
