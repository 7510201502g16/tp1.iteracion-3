package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by matias on 11/2/15.
 */
public class TodayFunction implements Function {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalArgumentException {
        if (!argument.equals("")) {
            throw new IllegalArgumentException();
        }
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(date);
    }
}
