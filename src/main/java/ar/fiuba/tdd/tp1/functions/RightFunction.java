package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

/**
 * Created by matias on 11/2/15.
 */
public class RightFunction extends SubstringFunction {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalArgumentException {
        super.validateArguments(argument, helper);
        return this.firstArgument.substring(this.firstArgument.length() - this.secondArgument);
    }
}
