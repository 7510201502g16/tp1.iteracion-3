package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;

/**
 * Created by fran on 15/10/15.
 */
public interface Function {
    String operate(String argument, DependencyHelper helper) throws IllegalArgumentException;
}
